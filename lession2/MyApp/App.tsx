import { StyleSheet, Text, View } from 'react-native';
import RootApp from '~/views/publics/RootApp';

export default function App() {
  return (
    <RootApp title='App Root' />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
