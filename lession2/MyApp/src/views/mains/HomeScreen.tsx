import { View, Text, Button } from "react-native";
import RootNavigation from "../publics/RootNavigation";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreenDetail from "./HomeScreenDetail";

const Stack = createNativeStackNavigator();
const HomeScreen = () => {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Đây là Home Screen</Text>
            <Button title="Go to detail"
                onPress={() => RootNavigation.navigate("Details")}
            />
        </View>
    )
}

const HomeStackScreen = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Home" component={HomeScreen} />
            <Stack.Screen name="Details" component={HomeScreenDetail} />
        </Stack.Navigator>
    );
}

const HomeScreenDrawer = () => {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Button title="Go to notifications"
                onPress={() => RootNavigation.navigate("Notifications")}
            />
        </View>
    )
}


export const Home = { HomeScreen, HomeStackScreen, HomeScreenDrawer };
