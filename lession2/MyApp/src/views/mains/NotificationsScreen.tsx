import { Button, View, Text } from "react-native"
import RootNavigation from "../publics/RootNavigation"


const NotificationsScreen = () => {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >
            <Button
                title="Back to home"
                onPress={() => RootNavigation.goBack()}
            ></Button>
        </View>
    )
}

export default NotificationsScreen;