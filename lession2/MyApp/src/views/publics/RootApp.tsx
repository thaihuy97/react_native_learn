import { View, Text } from "react-native"
import { Home } from "../mains/HomeScreen";
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Setting } from "../mains/SettingScreen";
import Ionicons from 'react-native-vector-icons/Ionicons';
import RootNavigation from "./RootNavigation";
import { createDrawerNavigator } from '@react-navigation/drawer';
import NotificationsScreen from "../mains/NotificationsScreen";
import * as React from 'react';


type IRootApp = {
    title: string
}

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const RootApp = (props: IRootApp) => {
    const { title } = props;
    return (
        <NavigationContainer ref={RootNavigation.navigationRef}>
            {/* <Tab.Navigator
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                        let iconName = "";
                        if (route.name === "Home") {
                            iconName = focused
                                ? 'ios-information-circle'
                                : 'ios-information-circle-outline';
                        }
                        else if (route.name === "Settings") {
                            iconName = focused
                                ? 'ios-list'
                                : 'ios-list-outline';
                        }
                        return <Ionicons name={iconName} size={size} color={color} />;
                    },
                    tabBarActiveTintColor: 'red',
                    tabBarInactiveTintColor: 'gray',

                })}>
                <Tab.Screen name="Home" component={Home.HomeStackScreen} options={{ tabBarBadge: 99 }} />
                <Tab.Screen name="Settings" component={Setting.SettingsScreen} />
            </Tab.Navigator> */}
            <Drawer.Navigator useLegacyImplementation={true} initialRouteName="Home">
                <Drawer.Screen name="Home" component={Home.HomeScreenDrawer} />
                <Drawer.Screen name="Notifications" component={NotificationsScreen} />
            </Drawer.Navigator>

        </NavigationContainer>
    )
}

export default RootApp;