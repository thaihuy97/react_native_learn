import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import RootApp from "~/views/RootApp";

export default function App() {
  return (
    <RootApp />
  );
}

