import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";


const Tab = createBottomTabNavigator();


const Stack = createNativeStackNavigator();

const Drawer1 = () => {
    return (
        <Tab.Navigator
        >
            <Tab.Screen name={"Tab01"} component={Tab01} options={{
                title: "Tab01"
            }} />

            <Tab.Screen name={"Tab02"} component={Tab02} options={{
                title: "Tab02"
            }} />

        </Tab.Navigator>
    )
}