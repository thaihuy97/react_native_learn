import { View, Text, Button } from "react-native";
import RootNavigation from "../RootNavigation";
import MainHeader from "./MainHeader";
import { Center } from "native-base";
import { useGlobalStore } from "../stores/useGlobalStore";




const View03 = ({ navigation }: any) => {
    const { themeMode, setThemeMode } = useGlobalStore();


    const changeTheme = () => {
        setThemeMode("light");
    }
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            {/* <MainHeader title="Hello" type="MENU" /> */}
            <View style={{ flex: 1 }}>
                <Center>
                    <Text style={{ fontSize: 20 }}>View 03</Text>
                    <Button
                        title="Go to Details"
                        onPress={() => navigation.navigate("View04")}
                    />

                    <Button
                        title="change theme"
                        onPress={changeTheme}
                    />
                </Center>
            </View>
        </View>
    );
}

export default View03;