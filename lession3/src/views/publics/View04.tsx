import { View, Text, Button } from "react-native";
import RootNavigation from "../RootNavigation";
import { useNavigation } from "@react-navigation/native";
import MainHeader from "./MainHeader";




const View04 = () => {
    const navigation = useNavigation<any>();

    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <MainHeader title="ABCS" type="BACK" />
            <View style={{ flex: 1 }}>
                <Text>View04 </Text>
                <Button
                    title="Go to View01"
                    onPress={() => RootNavigation.navigationRef.navigate("View01")}
                />
            </View>
        </View>
    );
}

export default View04;