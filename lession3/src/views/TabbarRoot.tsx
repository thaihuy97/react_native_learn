import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import View01 from "./publics/View01";
import View02 from "./publics/View02";
import View03 from "./publics/View03";
import View04 from "./publics/View04";
import View05 from "./publics/View05";
import Header from "./Header";
import { useColorModeValue, useTheme } from 'native-base';


const Tab = createBottomTabNavigator();


const Stack = createNativeStackNavigator();




const Tab01 = () => {
    const {
        colors
    } = useTheme();
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: colors["violet"]["800"]
                }
            }} >
            <Stack.Screen name={"View01"} component={View01} options={{
                title: "View01",
                header: (props) => <Header {...props} />
            }} />

            <Stack.Screen name={"View02"} component={View02} options={{
                title: "View02",
                header: (props) => <Header {...props} />
            }} />
        </Stack.Navigator>
    )
}


const Tab02 = () => {
    const {
        colors
    } = useTheme();
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: colors["violet"]["800"]
                }
            }}>
            <Stack.Screen name={"View03"} component={View03} options={{
                title: "View03",
                header: (props) => <Header {...props} />
            }} />

            <Stack.Screen name={"View04"} component={View04} options={{
                title: "View04",
                header: (props) => <Header {...props} />
            }} />
        </Stack.Navigator>
    )
}

const TabbarRoot = () => {
    return (
        <Tab.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <Tab.Screen name={"Tab01"} component={Tab01} options={{
                title: "Tab01",
            }} />

            <Tab.Screen name={"Tab02"} component={Tab02} options={{
                title: "Tab02"
            }} />

        </Tab.Navigator>
    )
}

export default TabbarRoot;