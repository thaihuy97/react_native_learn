

import { DrawerContentComponentProps } from "@react-navigation/drawer";
import { View, Text, Button } from "react-native";
import { useNavigation } from "@react-navigation/native";



const menus: { targetView: string }[] = [
    {
        targetView: "View01"
    },
    {
        targetView: "View02"
    },
    {
        targetView: "View03"
    },
    {
        targetView: "View04"
    },
]

const MenuView = (props: DrawerContentComponentProps) => {
    const navigation = useNavigation<any>();
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            {menus.map(({ targetView }) => (<Button
                key={targetView}
                title={targetView}
                onPress={() => {
                    navigation.navigate(targetView)
                }}
            />))}
        </View>
    );
}

export default MenuView;